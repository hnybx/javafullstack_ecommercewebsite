package com.huiproject.springbootecommerce.dao;

import com.huiproject.springbootecommerce.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "productCategory", path = "product-category") //first string: name of Json entry, second string: path
public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Long>{
}
